// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VRTTimerComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTimerTickElapsed, float, TimeElapsed, float, TimeLeft);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTimerCompleted, float, TimeElapsed, float, TimeLeft);

/**
 * VRTTimerComponent is a component for quiz-timer logic.
 */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VRTCOUNTDOWNCLOCKS_API UVRTTimerComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// The total time of the timer
	UPROPERTY(EditAnywhere)
	int TotalTimeSeconds = 60;

	// The time interval between timer updates
	UPROPERTY(EditAnywhere)
	float UpdateIntervalSeconds = 1;
	
	// Starts or resumes the timer
	UFUNCTION(BlueprintCallable, Category = "VRT Timer")
	void StartTimer();

	// Pause the timer
	UFUNCTION(BlueprintCallable, Category = "VRT Timer")
	void PauseTimer();

	// Reset the timer
	UFUNCTION(BlueprintCallable, Category = "VRT Timer")
	void ResetTimer();

	// Get Time Left (in seconds)
	UFUNCTION(BlueprintCallable, Category = "VRT Timer")
	float GetTimeLeft() const;
	
	// Event that is broadcast at each timer tick
	UPROPERTY(BlueprintAssignable, Category = "VRT Timer")
	FOnTimerTickElapsed OnTimerTickElapsed;

	// Event that is broadcast when timer completes
	UPROPERTY(BlueprintAssignable, Category = "VRT Timer")
	FOnTimerCompleted OnTimerCompleted;
	
	// Sets default values for this component's properties
	UVRTTimerComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	bool bIsTimerRunning = false;
	
	FTimerHandle TimerHandle;

	float TotalTimeElapsed = 0.0;

	void TickElapsed();
};
