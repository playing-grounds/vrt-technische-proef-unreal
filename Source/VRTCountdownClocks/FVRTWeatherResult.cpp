﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "FVRTWeatherResult.h"

FVRTWeatherResult::FVRTWeatherResult()
{
	this->Temperature = 0;
	this->Pressure = 0;
	this->Humidity = 0;
	this->Descriptions = TArray<FString>();
}

FVRTWeatherResult::FVRTWeatherResult(double Temperature, double Pressure, double Humidity, TArray<FString> Descriptions)
{
	this->Temperature = Temperature;
	this->Pressure = Pressure;
	this->Humidity = Humidity;
	this->Descriptions = Descriptions;
}
