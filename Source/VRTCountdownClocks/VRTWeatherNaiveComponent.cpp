// Fill out your copyright notice in the Description page of Project Settings.


#include "VRTWeatherNaiveComponent.h"

#include "HttpModule.h"
#include "Interfaces/IHttpResponse.h"

void UVRTWeatherNaiveComponent::PerformRequest()
{
	if (!IsRequestValid())
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not perform weather request. Inputs are invalid."));
		OnWeatherResultError.Broadcast();
		return;
	}

	const FString Url = BaseUrl + FString("?lat=")
	+ FString::SanitizeFloat(Latitude)
	+ FString("&lon=")
	+ FString::SanitizeFloat(Longitude)
	+ FString("&appId=")
	+ ApiKey;
	
	FHttpModule& HttpModule = FHttpModule::Get();
	const TSharedRef<IHttpRequest, ESPMode::ThreadSafe> Request = HttpModule.CreateRequest();
	Request->SetVerb(TEXT("GET"));
	Request->SetHeader("User-Agent", "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", "application/json");
	Request->SetURL(Url);

	Request->OnProcessRequestComplete().BindUObject(this, &UVRTWeatherNaiveComponent::OnRequestComplete);
	Request->ProcessRequest();
}

void UVRTWeatherNaiveComponent::OnRequestComplete(FHttpRequestPtr Request, FHttpResponsePtr Response, const bool bWasSuccessful) const
{
	if (!bWasSuccessful)
	{
		OnWeatherResultError.Broadcast();
		return;
	}

	const FString ResponseBody = Response->GetContentAsString();
	const TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(ResponseBody);
	TSharedPtr<FJsonObject> JsonObject;
	if(!FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		OnWeatherResultError.Broadcast();
		return;
	}

	const TArray<TSharedPtr<FJsonValue>>* WeatherDescriptionArray;
	if (!JsonObject->TryGetArrayField("weather", WeatherDescriptionArray))
	{
		OnWeatherResultError.Broadcast();
		return;
	}

	TArray<FString> WeatherDescriptions;
	for (const TSharedPtr<FJsonValue> WeatherValue : *WeatherDescriptionArray)
	{
		const TSharedPtr<FJsonObject> WeatherValueObject = WeatherValue->AsObject();
		FString WeatherDescription;
		if (!WeatherValueObject->TryGetStringField("main", WeatherDescription))
		{
			OnWeatherResultError.Broadcast();
			return;
		}
		WeatherDescriptions.Add(WeatherDescription);
	}

	const TSharedPtr<FJsonObject>* JsonMain;
	if (!JsonObject->TryGetObjectField("main", JsonMain))
	{
		OnWeatherResultError.Broadcast();
		return;
	}

	double WeatherTempKelvin;
	double WeatherPressure;
	double WeatherHumidity;

	if (
		!(*JsonMain)->TryGetNumberField("temp", WeatherTempKelvin) ||
		!(*JsonMain)->TryGetNumberField("pressure", WeatherPressure) ||
		!(*JsonMain)->TryGetNumberField("humidity", WeatherHumidity))
	{
		OnWeatherResultError.Broadcast();
		return;
	}

	const FVRTWeatherResult WeatherResult = FVRTWeatherResult(
		WeatherTempKelvin - 273.15,
		WeatherPressure,
		WeatherHumidity,
		WeatherDescriptions);

	OnWeatherResultReceived.Broadcast(WeatherResult);
}

// Sets default values for this component's properties
UVRTWeatherNaiveComponent::UVRTWeatherNaiveComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UVRTWeatherNaiveComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UVRTWeatherNaiveComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UVRTWeatherNaiveComponent::IsRequestValid() const
{
	return !ApiKey.IsEmpty();
}

