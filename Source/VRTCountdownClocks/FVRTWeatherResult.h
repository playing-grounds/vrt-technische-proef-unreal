﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "FVRTWeatherResult.generated.h"

/**
 * 
 */

USTRUCT(BlueprintType) struct FVRTWeatherResult
{
	GENERATED_BODY()

	FVRTWeatherResult();

	FVRTWeatherResult(double Temperature, double Pressure, double Humidity, TArray<FString> Descriptions);

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VRT Weather")
	double Temperature;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VRT Weather")
	double Pressure;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VRT Weather")
	double Humidity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VRT Weather")
	TArray<FString> Descriptions;
};
