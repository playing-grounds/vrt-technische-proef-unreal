// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FVRTWeatherResult.h"
#include "Components/SceneComponent.h"
#include "HttpModule.h"
#include "VRTWeatherNaiveComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeatherResultReceived, FVRTWeatherResult, WeatherResult);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeatherResultError);

/**
 * UVRTWeatherNaiveComponent is an Actor Component that fetches the wether directly from OpenWeatherMap
 * */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VRTCOUNTDOWNCLOCKS_API UVRTWeatherNaiveComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// OpenWeatherMap API Key
	UPROPERTY(EditAnywhere)
	FString ApiKey;

	// Latitude coordinate for weather request
	UPROPERTY(EditAnywhere)
	float Latitude;

	// Longitude coordinate for weather request
	UPROPERTY(EditAnywhere)
	float Longitude;

	// Function that triggers REST-request to OpenWeatherMap
	UFUNCTION(BlueprintCallable, Category = "VRT Weather")
	void PerformRequest();

	// Event that is broadcast when weather has been received
	UPROPERTY(BlueprintAssignable, Category = "VRT Weather")
	FOnWeatherResultReceived OnWeatherResultReceived;

	// Event that is broadcast when weather API returns an error
	UPROPERTY(BlueprintAssignable, Category = "VRT Weather")
	FOnWeatherResultError OnWeatherResultError;
	
	// Sets default values for this component's properties
	UVRTWeatherNaiveComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	FString BaseUrl = TEXT("https://api.openweathermap.org/data/2.5/weather");

	bool IsRequestValid() const;
	
	void OnRequestComplete(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful) const;
	
};
