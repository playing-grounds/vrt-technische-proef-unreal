// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VRTCountdownClocksGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class VRTCOUNTDOWNCLOCKS_API AVRTCountdownClocksGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
