// Copyright Epic Games, Inc. All Rights Reserved.

#include "VRTCountdownClocks.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, VRTCountdownClocks, "VRTCountdownClocks" );
