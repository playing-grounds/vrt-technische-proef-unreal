﻿#include "Misc/AutomationTest.h"
#include "Tests/AutomationCommon.h"
#include "VRTCountdownClocks/VRTTimerComponent.h"

DEFINE_LATENT_AUTOMATION_COMMAND_ONE_PARAMETER(FVRTTimerTestStartTimer, UVRTTimerComponent*, VRTTimerComponent);
bool FVRTTimerTestStartTimer::Update()
{
	if (VRTTimerComponent == nullptr)
	{
		return false;
	}
	VRTTimerComponent->TotalTimeSeconds = 10;
	VRTTimerComponent->StartTimer();
	return true;
}

DEFINE_LATENT_AUTOMATION_COMMAND_TWO_PARAMETER(FVRTTimerTestEnsureTimeleft, FAutomationTestBase*, Test, UVRTTimerComponent*, VRTTimerComponent);
bool FVRTTimerTestEnsureTimeleft::Update()
{
	Test->TestTrue("Time left should count down as VRT Timer progresses", VRTTimerComponent->GetTimeLeft() <= 8);
	return true;
}

IMPLEMENT_SIMPLE_AUTOMATION_TEST(VRTTimerComponentTest, "VRT.VRTCountdownClocks.VRTTimerComponentTest",
                                 EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)
bool VRTTimerComponentTest::RunTest(const FString& Parameters)
{
	// Attach VRT Timer Component to Dummy Actor
	const FWorldContext* WorldContext = GEngine->GetWorldContextFromGameViewport(GEngine->GameViewport);
	UWorld* World=WorldContext->World();
	AActor* SubjectActor = World->SpawnActor<AActor>();
	UActorComponent* SubjectComponent = SubjectActor->AddComponentByClass(UVRTTimerComponent::StaticClass(), false, FTransform::Identity, false);
	UVRTTimerComponent* VRTTimerComponent = Cast<UVRTTimerComponent>(SubjectComponent);

	ADD_LATENT_AUTOMATION_COMMAND(FEngineWaitLatentCommand(0.2f));
	// Start Timer
	ADD_LATENT_AUTOMATION_COMMAND(FVRTTimerTestStartTimer(VRTTimerComponent));
	ADD_LATENT_AUTOMATION_COMMAND(FEngineWaitLatentCommand(2.0f));
	// Assert
	ADD_LATENT_AUTOMATION_COMMAND(FVRTTimerTestEnsureTimeleft(this, VRTTimerComponent));
	
	return true;
}
