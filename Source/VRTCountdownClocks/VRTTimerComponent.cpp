// Fill out your copyright notice in the Description page of Project Settings.


#include "VRTTimerComponent.h"

void UVRTTimerComponent::StartTimer()
{
	// Do not start timer if it is already running
	if (bIsTimerRunning) return;
	
	const UWorld* World = GetWorld();
	
	if (World == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("World not found."))
		return;
	}

	World->GetTimerManager().SetTimer(TimerHandle, this, &UVRTTimerComponent::TickElapsed, UpdateIntervalSeconds, true);
	bIsTimerRunning = true;
}

void UVRTTimerComponent::PauseTimer()
{
	const UWorld* World = GetWorld();
	if (World == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("World not found."))
		return;
	}
	World->GetTimerManager().ClearTimer(TimerHandle);
	bIsTimerRunning = false;
}

void UVRTTimerComponent::ResetTimer()
{
	const UWorld* World = GetWorld();
	
	if (World == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("World not found."))
		return;
	}
	
	TotalTimeElapsed = 0;
	
	World->GetTimerManager().ClearTimer(TimerHandle);
	bIsTimerRunning = false;
}

float UVRTTimerComponent::GetTimeLeft() const
{
	return TotalTimeSeconds - TotalTimeElapsed;
}

// Sets default values for this component's properties
UVRTTimerComponent::UVRTTimerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UVRTTimerComponent::BeginPlay()
{
	Super::BeginPlay();

	
	// ...
	
}

void UVRTTimerComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
}


// Called every frame
void UVRTTimerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UVRTTimerComponent::TickElapsed()
{
	TotalTimeElapsed += UpdateIntervalSeconds;
	if (TotalTimeElapsed >= TotalTimeSeconds)
	{
		OnTimerCompleted.Broadcast(TotalTimeElapsed, TotalTimeSeconds - TotalTimeElapsed);
	}
	else
	{
		OnTimerTickElapsed.Broadcast(TotalTimeElapsed, TotalTimeSeconds - TotalTimeElapsed);
	}
}

